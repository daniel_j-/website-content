<h1> IRC Bot made in both Python and Java </h1>
<h3> 7/1/2016 </h3>
<p> I help maintain and moderate (when it was relevant, now we just have fun) an IRC channel dedicated to user support for the game BeamNG.drive (<a href="http://beamng.com">www.beamng.com</a>) located on the IRC server irc.beamng.com #BeamNG. If you have ever done any form of tech-support you know that the questions you receive are repetitive, very repetitive, and I for one, could only repeat myself so many times before I just had to come up with something easier.<br>
An IRC bot was a great idea - there are literally thousands online and opensource - the most favourable being EggDrop. I however have integrity and cannot simply use such a thing to make my life easier. Instead, I opted to make one, well multiple, for myself.<br>
Both of my final copies can be found here: <a href="https://github.com/daniel-Jones/BeamBot">https://github.com/daniel-Jones/BeamBot</a> <br>
One is programmed in Python, while the other in Java.<br>
Both bots work in the same simple manner: <br>
	<ul>
	<li>Bot is started via a console</li>
	<li>It creates a socket connection to the IRC server, it then joins your selected channel and idles</li>
	<li>The bot runs enters a loop that both checks for messages, logs the channel, checks for certain key word commands and replies to PING/PONG</li>
	<li>When the bot finds a line that begins with '?' a file full of commands is opened and parsed one by one to find valid commands and provide a channel response</li>
	</ul>
<br>
The bots are pretty simple, but has saved myself and others plenty of time. <br>
My IRC nick is daniel_j I can be found at the following locations, why not say hi:
	<ul>
	<li>irc.danieljon.es #fun</li>
	<li>irc.beamng.com #beamng, #offtopic, #speedsims</li>
	</ul>
  
