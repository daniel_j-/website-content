<h1>qutebrowser hints for Reddit expand buttons</h1>
<h3>13/2/2018</h3>
Reddit expand buttons are not by default hinted. Customisable hinting for custom elements is not yet implemented (see <a href="https://github.com/qutebrowser/qutebrowser/issues/2773">#2773</a>). So as a temporary work around I have replaced 'SELECTORS{...}' in <i>qutebrowser/browser/webelem.py</i> to:
<pre>
SELECTORS = {
	Group.all: (	'a, area, textarea, select, input:not([type=hidden]), button, '
			'frame, iframe, link, [onclick], [onmousedown], [role=link], '
			'[role=option], [role=button], img, '
			# Angular 1 selectors
			'[ng-click], [ngClick], [data-ng-click], [x-ng-click],'
			# Reddit expando selectors
			'div[class="expando-button collapsed hide-when-pinned selftext"], div[class="expando-button hide-when-pinned selftext expanded"],'
			'div[class="expando-button collapsed hide-when-pinned video"], div[class="expando-button hide-when-pinned video expanded"],'
			'div[class="expando-button collapsed hide-when-pinned crosspost"], div[class="expando-button hide-when-pinned crosspost expanded"]'),
	Group.links: 	'a[href], area[href], link[href], [role=link][href]',
	Group.images: 	'img',
	Group.url: 	'[src], [href]',
	Group.inputs: ( 'input[type=text], input[type=email], input[type=url], '
			'input[type=tel], input[type=number], '
			'input[type=password], input[type=search], '
			'input:not([type]), textarea'),
	}

</pre>
Allowing me to use hinting on Reddit expand elements.
